var li_header = document.querySelectorAll('nav > ul > li')

window.onload = init()

function init(){
    console.log('DOM is ready')

    // ADD EVENT LISTENERS
    for (var i = 0; i < li_header.length; i++) {
        li_header[i].addEventListener('click', changePage)
    }
}

function changePage() {
    document.querySelector('nav > ul > li.active').classList.remove('active')

    setTimeout(function(){
        document.querySelector('nav > ul > li.reversed').classList.remove('reversed')
        this.classList.add('active')
    }.bind(this), 300)

    setTimeout(function(){
        this.classList.add('reversed')
    }.bind(this), 300)
}

function switch_tab(){
    console.log(this)
}
