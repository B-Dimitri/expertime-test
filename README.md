# Test pour Expertime

Bonjour, je suis Dimitri Bahuaud et j'ai réalisé un test pour Expertime,
pour le poste de développeur Front-End.

# Un exemple ?
Après le test, j'ai essayé de chercher le site sur Internet, pour voir les solutions choisies pour ce site. Je ne l'ai malheureusement pas trouvé, aussi, je serais curieux, si c'est possible, d'avoir un exemple de cette réalisation.

# Les difficultés
## Les typographies
N'ayant pas su les exporter depuis Figma, et les fonts étant payantes, j'ai récupéré des équivalents.
J'ai travaillé les fichiers comme si j'avais la Canela et la Styrenne B en ma possession.

## Les librairies/pré-processeurs
Cela faisait quelque temps que je n'avais pas travaillé en CSS pur.
J'ai préféré recréer des classes habituellement utilisées dans certaines librairies dans le fichier 'style.css'.

## Le troisième bloc
Je n'ai pas pu finir ce troisième bloc, néanmoins, je trouve que c'est un exercice intéressant à fabriquer, je le réaliserais par la suite, pour moi.

## Le temps
En effet, j'aurais voulu montrer plus d'animation comme demandé dans le mail comme :
- du reveal on scroll
- faire un curseur animé selon les endroits cliquables du suite
- animer le slider du troisième block
J'ai préféré finir l'architecture et le rendre stable, y compris en responsive, avec le menu burger.

## .DS_STORE
Bien que ce ne soit pas forcément une difficulté, mon ordinateur personnel est un Mac.
Habituellement, je travaille sur git, avec un .git_ignore.
J'ai essayé de tous les enlever, mais.. Il se peut qu'il en reste à l'intérieur du zip envoyé.
